﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManagement : MonoBehaviour
{
    private Vector2 axis;
    private CharacterController controller;
    public float speed;
    public Vector3 moveDirection;
    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;

    private Vector3 transformDirection;
    public GameObject mano;
    public GameObject lanzacohetes;
    public int lives = 3;

    [SerializeField] private FireTemplate bullet;
    [SerializeField] SoundPlayer sound;
    [SerializeField] private FireTemplate propmisil;
    [SerializeField] ScoreManager scoreManager;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        if (controller.isGrounded && !jump)
        {
            moveDirection.y = Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }


        controller.Move(moveDirection * Time.deltaTime);
    }


    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

   /* public void Fire()
    {
        Debug.Log("Fire");

        // Instanciar una pelota
        FireTemplate pelota = Instantiate(bullet, mano.transform.position, mano.transform.rotation) as FireTemplate;

        // Ponerla en la posición del player

        // Dispararla
        pelota.Fire();
    }
    */
    //misil
    public void Fire2()
    {
        Debug.Log("Fire2");

        // Instanciar una pelota
        FireTemplate misil = Instantiate(propmisil, lanzacohetes.transform.position, lanzacohetes.transform.rotation) as FireTemplate;

        // Ponerla en la posición del player
        sound.Play(1, 100);
        // Dispararla
        misil.Fire();
    }
    //missil

    public void Jump()
    {
        if (!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }

    public void SetDead()
    {
        //sound.Play(0, 1);
        lives--;
        if (lives <= 0)
        {
            //animacio
            //particules
            Destroy(this.gameObject);
        }
        //state = State.Dead;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("flecha"))
        {
            lives--;
            sound.Play(3, 100);
            if (lives <= 0)
            {
                Destroy(this.gameObject);
                SceneManager.LoadScene("GameOver");
            }

            else if (collision.gameObject.CompareTag("flecha"))
            {
                scoreManager.LoseLife();
            }

        }

    }

  
}