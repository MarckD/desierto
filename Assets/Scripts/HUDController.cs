﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    [SerializeField] Text enemyCount;
    void Awake(){
        GameManager.Instance.updateUI += updateHUD;
    }
    void OnDestroy(){
        GameManager.Instance.updateUI -= updateHUD;
    }

    void updateHUD(){
        enemyCount.text = GameManager.Instance.enemyCount.ToString();
    }
}
