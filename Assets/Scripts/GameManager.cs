﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{    
    public static GameManager Instance = null;
    public int enemyCount;
    public Action killedEnemy;
    public Action updateUI;

    public static GameManager Instance2 = null;
    public int playerCount;
    public Action killedPlayer;
    public Action updateUI2;
    

    void Awake(){
        if (Instance == null) Instance = this;
        else Destroy(this);

        killedEnemy += updateEnemyCount;

        if (Instance2 == null) Instance2 = this;
        else Destroy(this);

        killedPlayer += updatePlayerCount;
    }

    void OnDestroy(){
        killedEnemy -= updateEnemyCount;

        killedPlayer -= updatePlayerCount;
    }

    void Start(){
        updateUI.Invoke();

        updateUI2.Invoke();
    }

    void updateEnemyCount(){
        enemyCount--;

        updateUI.Invoke();

        if (enemyCount == 0)
        {
            SceneManager.LoadScene("Win");
        }
    }
    void updatePlayerCount()
    {
        playerCount--;

        updateUI2.Invoke();

        if (playerCount == 0)
        {
            SceneManager.LoadScene("Win");
        }
    }
    

}
