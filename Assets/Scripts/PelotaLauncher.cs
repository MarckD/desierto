﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaLauncher : FireTemplate
{
    public float throughForce;

    public override void Fire()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }

    public override void DestroySelf()
    {
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision){
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<CreaperBehaviour>().SetDead();
        }
    }

}
