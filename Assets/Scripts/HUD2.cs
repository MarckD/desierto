﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD2 : MonoBehaviour
{
    [SerializeField] Text playerCount;
    void Awake()
    {
        GameManager.Instance.updateUI2 += updateHUD;
    }
    void OnDestroy()
    {
        GameManager.Instance.updateUI2 -= updateHUD;
    }

    void updateHUD()
    {
        playerCount.text = GameManager.Instance.playerCount.ToString();
    }
}
